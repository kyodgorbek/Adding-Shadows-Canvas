# Adding-Shadows-Canvas
Adding Shadow Canvas
 // CSS color shadow, accepts RGBA, RGB, hexadecimal
 ctx.shadowColor = "rgba(255,255,255,0.5)";
 
 ctx.shadowOffsetX = 4;     // horizontal shadow offset
 ctx.shadowOffsetY = 4;    // vertical shadow offset
 ctx.shadowBlur = 10;     // distance for shadow to fade out
